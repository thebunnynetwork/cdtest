# Conde Nast Assessment

## Caveats 

I expected more time to work on this assessment, but personal issues cut into the time I was able to block out for this assessment.

### Planned but not implemented
* Testing
* Server side, validation of the user provided inputs
* Client side, validation of the user provided inputs
* Something more interesting for the styling but I couldn't decide where to put the date on the card
* Adding a refresh button and way for user to easily return to main feed after searching
* Client side validating the return article data before attempting to use it
* Probably more but that's what comes to mind
* I didn't have time to test this deployed either or to attempt to install it on another machine, so I'm hopeful that I didn't add a machine specific dependency.

## How to run

Prerequisites: 
* Built on Node 14. Probably compatible Node 10-16.
* add a .env file in the /server directory with the following line: 
* `API_KEY=xxxxxxx`
* Replace the `xxxxxxx` with provided Newsapi.org API key

1. Start the server: 
   * `npm install` in the /server directory
   * `npm start` in the /server directory

2. Start the client: 
   * `npm install` in the /client directory
   * `npm start` in the /client directory

This is currently configured for the server to be running on localhost:3001 and the client application includes hardcoded calls to this. If changes are made to where the server is running, they need to be added to the App.js file. If this were a larger application, this would be in an environment file.


