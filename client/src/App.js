import React from 'react';
import logo from './logo.svg';
import './App.css';
import ArticleCard from './components/ArticleCard';
import SearchForm from './components/TextInput';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { articles: [] };

    this.search = this.search.bind(this);
  }

  componentDidMount() {
    fetch('http://localhost:3001/api/topnews')
      .then((res) => res.json())
      .then((data) => this.setState({ articles: data.articles }));
  }

  search(term) {
    fetch(`http://localhost:3001/api/topnews?search=${term}`, { method: 'POST' })
      .then((res) => res.json())
      .then((data) => this.setState({ articles: data.articles }));
  }

  render() {
    return (
      <div className="App">
        {!this.state.articles
          ? (
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <p>Loading...</p>
            </header>
          )
          : (
            <div>
              <SearchForm search={this.search} />
              {this.state.articles.map((article) => (<ArticleCard article={article} />))}
            </div>
          )}
      </div>
    );
  }
}

export default App;
