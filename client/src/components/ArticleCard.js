import React from 'react';

const formatDate = (dateString) => {
  const date = new Date(dateString);
  return new Intl.DateTimeFormat('en-GB', {
    month: 'long',
    day: 'numeric',
    year: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }).format(date);
};
const ArticleCard = (props) => (
  <div className="article-card">
    <p>{formatDate(props.article.publishedAt)}</p>
    <img src={props.article.urlToImage} alt={`Cover for ${props.article.title}`} />
    <h2><a href={props.article.url} target="_blank" rel="noreferrer">{props.article.title}</a></h2>
    <h4>
      {props.article.author}
      ,
      {' '}
      {props.article.source.name}
    </h4>
    <p>{props.article.description}</p>
  </div>
);

export default ArticleCard;
