const express = require("express");
const cors = require('cors');
const axios = require('axios');

require('dotenv').config();

const PORT = process.env.PORT || 3001;

const app = express();
app.use(cors());

app.get("/api/topnews",
    async (req, res) => {
        try {
            const response = await axios.get(`https://newsapi.org/v2/top-headlines?country=gb&apiKey=${process.env.API_KEY}`)
            res.json(response.data);
        } catch (error) {
            console.log(error.response);
        }
    }
);

app.post('/api/topnews',
   async (req, res) => {
       try {
           const response = await axios.get(`https://newsapi.org/v2/everything?q=${req.query.search}&apiKey=${process.env.API_KEY}`)
           res.json(response.data)
       } catch (error) {
           console.log(error.response);
       }
   }
);

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
});